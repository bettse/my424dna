const {aesCmac} = require("node-aes-cmac")

function test() {
  const uid = '04958CAA5C5E80';
  const counter = '080000';
  const dynamicFileData = 'CEE9A53E3E463EF1F459635736738962&cmac=';

  const SDMFileRead = Buffer.alloc(0x10).fill(0);


  const sv2 = Buffer.from('3CC300010080' + uid + counter, 'hex');
  const SesSDMFileReadMAC = aesCmac(SDMFileRead, sv2, {returnAsBuffer: true});


  const SDMMACfull = aesCmac(SesSDMFileReadMAC, dynamicFileData, {returnAsBuffer: true});

  const SesSDMFileReadMACControl = '3ED0920E5E6A0320D823D5987FEAFBB1';
  const SDMMACfullControl = '81EC45C175E72FF6FAC61BC7AB3BAEF6'
  console.log({
    SesSDMFileReadMAC: SesSDMFileReadMAC.toString('hex'),
    SesSDMFileReadMACControl,
    SDMMACfull: SDMMACfull.toString('hex'),
    SDMMACfullControl,
  })
}

function mact(mac) {
  const t = Buffer.alloc(8);
  for(var i = 0; i < mac.length; i++) {
    if (i % 2 == 1) {
      t[(i / 2) >>> 0] = mac[i];
    }
  }
  return t;
}

function validateCmac(url, offset, key) {
  const parsedUrl = new URL(url);
  const params = new URLSearchParams(parsedUrl.search);
  const counter = parseInt(params.get('ctr'), 0x10);
  const c = params.get('c');
  const uid = params.get('uid')
  const dynamicFileData = url.slice(offset, url.length - 16)

  const sv2 = Buffer.from('3CC300010080' + uid + '000000', 'hex');
  sv2.writeUIntLE(counter, 13, 3);
  const SesSDMFileReadMAC = aesCmac(key, sv2, {returnAsBuffer: true});
  const SDMMACfull = aesCmac(SesSDMFileReadMAC, dynamicFileData, {returnAsBuffer: true});
  const t = mact(SDMMACfull);

  console.log({
    dynamicFileData,
    SesSDMFileReadMAC: SesSDMFileReadMAC.toString('hex'),
    SDMMACfull: SDMMACfull.toString('hex'),
    t: t.toString('hex'),
    c,
  })
  return c.toLowerCase() === t.toString('hex').toLowerCase();
}

const url = 'https://my424dna.com/?uid=0469892A506380&ctr=0000A6&c=73FEA96B88713C95'
if (validateCmac(url, 20, Buffer.alloc(0x10).fill(0))) {
  console.log('cmac valid!');
} else {
  console.log('cmac NOT valid');
}
