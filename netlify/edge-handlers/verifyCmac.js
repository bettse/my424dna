const {aesCmac} = require("node-aes-cmac")

const { NETLIFY_DEV } = process.env;

function validateCmac(url, offset, key) {
  const parsedUrl = new URL(url);
  const params = new URLSearchParams(parsedUrl.search);
  const counter = parseInt(params.get('ctr'), 0x10);
  const c = params.get('c');
  const uid = params.get('uid')
  const dynamicFileData = url.slice(offset, url.length - 16)

  const sv2 = Buffer.from('3CC300010080' + uid + '000000', 'hex');
  sv2.writeUIntLE(counter, 13, 3);
  const SesSDMFileReadMAC = aesCmac(key, sv2, {returnAsBuffer: true});
  const SDMMACfull = aesCmac(SesSDMFileReadMAC, dynamicFileData, {returnAsBuffer: true});
  const t = mact(SDMMACfull);

  console.log({
    dynamicFileData,
    SesSDMFileReadMAC: SesSDMFileReadMAC.toString('hex'),
    SDMMACfull: SDMMACfull.toString('hex'),
    t: t.toString('hex'),
    c,
  })
  return c.toLowerCase() === t.toString('hex').toLowerCase();
}

// Log every incoming request URL
export function onRequest(event) {
  const { requestMeta } = event;
  const { url, headers } = requestMeta;
  console.log(`Incoming request for ${url}`);
  if (NETLIFY_DEV) {
    validateCmac(url, 21, Buffer.alloc(0x10).fill(0))
  } else {
    validateCmac(url, 20, Buffer.alloc(0x10).fill(0))
  }
  event.replaceResponse(
    new Response("<h1>Access denied</h1>", {
      headers: {
        "content-type": "text/html",
      },
      status: 404,
    })
  );
}
